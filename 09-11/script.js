let minha_variavel = [1,2,3,4,'casa'];
let outra_variavel = [1,2,3,4]
const nova_variavel = [
    {
        nome: 'um',
        valor: 1,
        valor_real: 1.0
    },
    {
        nome: 'dois',
        valor: 2,
        valor_real: 2.0
    },
    {
        nome: 'três',
        valor: 3,
        valor_real: 3.0
    },
];

const imprime = (entrada) => {
    let saida = "";
    for (let i=0; i< entrada.length; i++){
        saida = saida + entrada[i];
        if (i != entrada.length - 1) {
            saida = saida + '-'
        }
    }

    return saida + ".";
}

const imprimeForEach = (entrada) => {
    let saida = "";
    entrada.forEach( (elemento, indice) => {
        saida += elemento;
        if (indice < entrada.length - 1) saida += "-";
    });

    return saida + ".";
}

const imprimeForOf = (entrada) => {
    let saida = "";
    let count = 0;
    for (const elemento of entrada){
        saida += (count < entrada.length - 1)? elemento + '-': elemento;
        count++;
    }

    return saida + ".";
}

const incluiImagem = (entrada) => {
    return entrada.map((elemento, indice) => {
        elemento.imagem = 'numero_' + (indice + 1) + '.jpg';
        return elemento;
    })
}

const filtraLista = (lista, pesquisa) => {
    let nome_limpo;
    let deve_entrar;

    const nova_lista =  lista.filter( (elemento)=>{
        nome_limpo = elemento.nome.toLowerCase();
        deve_entrar = nome_limpo.includes(pesquisa.toLowerCase());
        return deve_entrar;
    });
}

const cria_card = () =>{
    const lista = incluiImagem(nova_variavel);
    const elemento = lista[0];
    const minha_div = document.querySelector("#div");

    minha_div.innerHTML += `<img src='imagens/${elemento.imagem} '>`;
    minha_div.innerHTML += `<h3> ${elemento.nome} </h3>`;
    minha_div.innerHTML += `<p> ${elemento.valor} </p>`;
    minha_div.innerHTML += `<p> ${elemento.valor_real} </p>`;

}

const cria_card_criando_elementos = (elemento) =>{
    const minha_div = document.querySelector('#div');

    const img = document.createElement('img');
    img.src = `imagens/${elemento.imagem}`;
    minha_div.appendChild(img);

    const h3 = document.createElement('h3');
    h3.innerHTML = elemento.nome;
    minha_div.appendChild(h3);

    const valor = document.createElement('p');
    valor.innerHTML = elemento.valor;
    minha_div.appendChild(valor);

    const valor_real = document.createElement('p');
    valor_real.innerHTML = elemento.valor_real;
    minha_div.appendChild(valor_real);
}

window.onload = () =>{
    const lista = incluiImagem(nova_variavel);
    lista.forEach((e) => {
        cria_card_criando_elementos(e);
    })
}


