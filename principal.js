const meu_vetor = ['primeiro elemento', 21];

console.log('meu_vetor.length', meu_vetor, length);

let outro_vetor = new Array();
console.log('outro_vetor.length', outro_vetor.length);
outro_vetor[0] = 'Outro primeiro elemento';
outro_vetor[1] = 7;
outro_vetor[2] = 37;
const func_while = ( entrada ) => {
    let contador = 0;
    while (contador < entrada.length){
        console.log(entrada[contador])
        contador += 1;
    }
}

function func_for( entrada ){
    let achou = true;
    for(let i = 0; i < entrada.length; i++){
        if (entrada[i] > 30){
            console.log(entrada[i]);
            achou = true;
        }
    }

    if(!achou) {
        console.log("Não achei valor assim");
    }
}